//requires
const express = require('express'),
      bodyParser = require('body-parser'),
      f = require("./function");

//const
const port = 3002;
const rute = '/app'

//APP
var app = express();
app.set('port', port);
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(bodyParser.raw());
app.use(express.urlencoded({extended: false}));


app.get(rute, function(req, res) {
    var respond = {"type":"get"}
    f.respond(res , respond)
});
app.post(rute, function(req, res) {
    var respond = {"type":"post"}
    f.respond(res , respond)
});
app.put(rute, function(req, res) {
    var respond = {"type":"put"}
    f.respond(res , respond)
});
app.delete(rute, function(req, res) {
    var respond = {"type":"delete"}
    f.respond(res , respond)
});

app.listen(port, function() {
    f.list()
    console.log('ok');
});