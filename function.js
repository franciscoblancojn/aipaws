
//obj functions
var f = {}

f.respond = (res , responde) => {
    res.setHeader('Content-Type', 'application/json');
    res.send(JSON.stringify(responde));
    res.flush()
    res.end();
}

module.exports = f;